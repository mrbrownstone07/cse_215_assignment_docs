package app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * StudentContainer
 */
public class StudentContainer implements Iterable<Student> {
    private ArrayList<Student> sList = new ArrayList<Student>();

    public void addStudent(Student e) {
        sList.add(e);
    }

    public void removeStudent(int id) {
        sList.removeIf(s -> s.id == id);
    }

    public void sortByMarks(){
        Collections.sort(sList, Student.marksComparator);
    }

    public void sortById(){
        Collections.sort(sList, Student.idComparator);
    }

    @Override
    public Iterator<Student> iterator() {
        return this.sList.iterator();
    }

}