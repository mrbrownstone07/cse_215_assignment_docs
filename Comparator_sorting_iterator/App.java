package app;

/**
 * App
 */
import java.util.*;

public class App {
    public static void main(String[] args) throws Exception {
        StudentContainer sc = new StudentContainer();

        sc.addStudent(new Student("Rafin", 172, 20));
        sc.addStudent(new Student("Rafiq", 173, 50));
        sc.addStudent(new Student("Rakib", 171, 10));
        sc.addStudent(new Student("Rafid", 174, 16));
        sc.addStudent(new Student("Rabib", 175, 60));
        sc.addStudent(new Student("Radit", 170, 10));

        sc.sortById();
        for (Student s : sc) System.out.println(s.name + "->" + s.id);

        sc.sortByMarks();
        for (Student s : sc) System.out.println(s.name + "->" + s.marks);
    }
}