package app;

/**
 * Student
 */
import java.util.Comparator;

public class Student{
    public String name;
    public int id;
    public double marks;
    
    Student(String name, int id, double marks){
        this.name = name;
        this.id = id;
        this.marks = marks;
    }
    
    public static Comparator<Student> marksComparator = new Comparator<Student>(){
        public int compare(Student s1, Student s2){
            return (s1.marks > s2.marks) ? 1 : (s1.marks < s2.marks) ? -1 : 0;
        }
    };
    
    public static Comparator<Student> idComparator = new Comparator<Student>(){
        public int compare(Student s1, Student s2){
            return (s1.id > s2.id) ? 1 : (s1.id < s2.id) ? -1 : 0;
        }
    };
}